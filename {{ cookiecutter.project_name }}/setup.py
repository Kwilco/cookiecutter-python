import setuptools


setuptools.setup(
    name='{{ cookiecutter.project_name }}',
    version='0.1.0',
    description='{{ cookiecutter.project_description }}',
    author='{{ cookiecutter.author }}',
    author_email='{{ cookiecutter.author_email }}',
    packages=setuptools.find_packages(),
    include_package_data=True,
    entry_points={
        'console_scripts': [
        ],
    },
    install_requires=(
    ),
)
